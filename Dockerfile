FROM openjdk:11

ENV SPRING_PROFILES_ACTIVE=H2 

WORKDIR /app

COPY target/assignment-*.jar /app 

ENTRYPOINT ["sh", "-c", "java -jar -Dserver.port=8070 -Dspring.profiles.active=h2 assignment-*.jar" ]

